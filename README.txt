Topic Map Exporter is a configurable exporter that goes trough entire Drupal
site and generates a topic map of it. The format of the exported topic map is
XTM 2.0.


Installation
------------

This module has no special requirements for the installation.
See this url for generally installing contributed modules:
http://drupal.org/documentation/install/modules-themes/modules-7

Usage
-----
1. Go to the export page:
   http://your_drupal_site/admin/reports/topic-map-export
2. Select export preset or configure by hand which entities, bundles and
   associations you wish to export. Then submit.
3. After the export process is done. You should get message which has a link
   the exported file. If not, you can find file in:
   sites/default/files/topicmap_{timestamp}.xtm

Author
------
Elias Tertsunen (Grip Studios Interactive Oy)
elias@gripstudios.com
