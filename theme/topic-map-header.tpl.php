<?php
/**
 * @file
 * Header for the topic map and also the "root" topic.
 */
?>
<?php echo '<?xml version="1.0" encoding="utf-8"?>'; ?>

<topicMap version="2.0">
  <topic id="website">
    <name>
      <value><?php echo $site_name ?></value>
    </name>
  </topic>
